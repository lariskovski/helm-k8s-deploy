## Helm Gitlab Pages K8s Deploy Example

Simples example on how to deploy a Helm chart hosted on Gitlab pages on a Kubernetes Cluster without direct integration with Gitlab (w/o Auto Devops).

The variables below are an example, needs to be changed acordingly:

~~~~
GOOGLE_APPLICATION_CREDENTIALS: /etc/deploy/gke-service-account.json
KUBECONFIG: /etc/deploy/kubeconfig.yaml
HELM_REPO_NAME: my-repo
HELM_CHART_NAME: test
HELM_REPO_URL:  https://lariskovski.gitlab.io/helm
~~~~